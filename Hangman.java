import java.util.Scanner;


public class Hangman{

	public static void main (String[] args){

		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter a 4 letter word that would have to be guessed! ");
		String word = reader.nextLine();
		
		runGame(word);
		
	}

	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if( toUpperCase( word.charAt(i) ) == toUpperCase(c) ){
				return i;
			}
			if(i == word.length()-1){
				return -1;
			}
		}
		return 0;
	}
	
	public static char toUpperCase(char c){
		c = Character.toUpperCase(c);
		return c;
	}
	
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String guessedWord = "";

		
		if(letter0 == true){
			guessedWord = guessedWord + word.charAt(0);
		}
		else{
			guessedWord = guessedWord + "-";
		}
		
		if(letter1 == true){
			guessedWord = guessedWord + word.charAt(1);
		}
		else{
			guessedWord = guessedWord + "-";
		}
		
		if(letter2 == true){
			guessedWord = guessedWord + word.charAt(2);
		}
		else{
			guessedWord = guessedWord + "-";
		}
		
		if(letter3 == true){
			guessedWord = guessedWord + word.charAt(3);
		}
		else{
			guessedWord = guessedWord + "-";
		}
			
		System.out.println(guessedWord);
	}
	
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		
	
		
		boolean victory = false;
		int numMisses = 0;
		
		while(victory == false && numMisses < 6){
			
			System.out.println("Try to guess a letter! Enter your choice in the command line.");
			char guess = reader.nextLine().charAt(0);

			int letterPos = isLetterInWord(word, guess);
			if( letterPos >= 0){
				System.out.println("Your guess was right! Continue like that and you might win this game!");

				if(letterPos == 0){
					letter0 = true;
				}
				
				if(letterPos == 1){
					letter1 = true;
				}
				
				if(letterPos == 2){
					letter2 = true;
				}
				
				if(letterPos == 3){
					letter3 = true;
				}
				
				if(letter0 == true && letter1 == true && letter2 == true && letter3 == true){
				victory = true;
				}
				
			}
			else{
				numMisses++;
				System.out.println("You made a mistake!Now you only have " + (6 - numMisses) + " chances left, be careful!");
			}
			
			System.out.println("Letter(s) found: " );
			printWord(word, letter0, letter1, letter2, letter3);
				
		}
		
		if(victory == true){
			System.out.println("Bravo, You WON the game!");
		}
		else{
			System.out.println("Too bad, you LOST the game!");
		}

	}
	
}